Fedora 25 - Package Repository
==============================

Repository Setup
----------------

Install with dnf repository.

```
% sudo tee -a /etc/yum.repos.d/kjdev.repo <<EOM
[kjdev]
name=Fedora \$releasever - kjdev
baseurl=https://gitlab.com/kjdev/fc\$releasever.pkg/raw/master/rpm/
enabled=1
gpgcheck=0

[kjdev-debuginfo]
name=Fedora \$releasever - kjdev - Debug
baseurl=https://gitlab.com/kjdev/fc\$releasever.pkg/raw/master/debug/
enabled=0
gpgcheck=0

[kjdev-source]
name=Fedora \$releasever - kjdev - Source
baseurl=https://gitlab.com/kjdev/fc\$releasever.pkg/raw/master/source/
enabled=0
gpgcheck=0
EOM
```

RPM Install
-----------

```
% dnf install <PACKAGE>
```
